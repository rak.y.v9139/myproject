package com.example.myproject.MyProject.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Entity
@Table(name = "executors")
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(name = "default_gen", sequenceName = "executors_seq", allocationSize = 1)
public class Executor extends GenericModel {
    @OneToOne
    private User user;
    @ManyToOne
    @JoinColumn(name = "Role_id", nullable = false,
            foreignKey = @ForeignKey(name = "FK_EXECUTOR_ROLES"))
    private Role role;
    @Column(name = "salary")
    private Integer salary;
    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinTable(name = "works_executors",
            joinColumns = @JoinColumn(name = "executor_id"), foreignKey = @ForeignKey(name = "FK_EXECUTORS_WORKS"),
            inverseJoinColumns = @JoinColumn(name = "work_id"), inverseForeignKey = @ForeignKey(name = "FK_WORKS_EXECUTORS"))
    private Set<Work> works;
}
