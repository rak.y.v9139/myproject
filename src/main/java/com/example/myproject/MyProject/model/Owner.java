package com.example.myproject.MyProject.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "owners")
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(name = "default_gen", sequenceName = "owners_seq", allocationSize = 1)

public class Owner extends GenericModel{
    @OneToOne
    private User user;
    @Column(name = "title")
    private String title;
    @Column(name = "company")
    private String company;
}
