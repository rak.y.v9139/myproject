package com.example.myproject.MyProject.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.context.annotation.EnableMBeanExport;

import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "orders")
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(name = "default_gen", sequenceName = "orders_seq", allocationSize = 1)
public class Order extends GenericModel {
    @ManyToOne
    @JoinColumn(name = "owner_id", nullable = false,
            foreignKey = @ForeignKey(name = "FK_ORDER_OWNER"))
    private Owner owner;
    @Column(name = "address")
    private String address;
    @Column(name = "start_order")
    private LocalDate startOrder;
    @Column(name = "finish_order")
    private LocalDate finishOrder;
    @ManyToOne
    @JoinColumn(name = "user_id",foreignKey = @ForeignKey(name = "FK_ORDER_USER"))
    private User user;
    @OneToMany(mappedBy = "order")
    private Set<Work> works;

    @ManyToOne
    @JoinColumn(name = "status_object", nullable = false,
            foreignKey = @ForeignKey(name = "FK_ORDER_STATUS"))
    private StatusOrder statusOrder;
}
