package com.example.myproject.MyProject.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "works")
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(name = "default_gen", sequenceName = "works_seq", allocationSize = 1)
public class Work extends GenericModel {
    @ManyToOne
    @JoinColumn(name = "work_id", nullable = false,
            foreignKey = @ForeignKey(name = "FK_WORK_ORDER"))
    private Order order;
    @Column(name = "start_work")
    private LocalDate startWork;

    @Column(name = "finish_work")
    private LocalDate finishWork;
    @Column(name = "price_work")
    private Integer priceWork;
    @ManyToOne
    @JoinColumn(name = "user_id",foreignKey = @ForeignKey(name = "FK_WORK_USER"))
    private User user;
    @ManyToMany
    @JoinTable(
            name = "works_executors",
            joinColumns = @JoinColumn(name = "work_id"), foreignKey = @ForeignKey(name = "FK_WORKS_EXECUTORS"),
            inverseJoinColumns = @JoinColumn(name = "executor_id"), inverseForeignKey = @ForeignKey(name = "FK_EXECUTORS_WORKS"))
    private Set<Executor> executors;
    @ManyToOne
    @JoinColumn(name = "status_work", nullable = false,
            foreignKey = @ForeignKey(name = "FK_WORK_STATUS"))
    private StatusWork statusWork;
}
