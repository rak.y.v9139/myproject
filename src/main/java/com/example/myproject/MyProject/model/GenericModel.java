package com.example.myproject.MyProject.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
public class GenericModel {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "default_gen")
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "created_when")
    private LocalDate createdWhen;

    @Column(name = "created_by")
    private String createdBy;
}
