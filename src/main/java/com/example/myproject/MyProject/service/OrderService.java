package com.example.myproject.MyProject.service;

import com.example.myproject.MyProject.dto.OrderDTO;
import com.example.myproject.MyProject.mapper.GenericMapper;
import com.example.myproject.MyProject.mapper.OrderMapper;
import com.example.myproject.MyProject.model.Order;
import com.example.myproject.MyProject.repository.GenericRepository;
import com.example.myproject.MyProject.repository.OrderRepository;
import org.springframework.stereotype.Service;

@Service
public class OrderService extends GenericService<Order, OrderDTO> {
    protected OrderService(OrderRepository orderRepository , OrderMapper orderMapper) {

        super(orderRepository,orderMapper);
    }
}
