package com.example.myproject.MyProject.service;

import com.example.myproject.MyProject.dto.UserDTO;
import com.example.myproject.MyProject.mapper.UserMapper;
import com.example.myproject.MyProject.model.User;
import com.example.myproject.MyProject.repository.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserService extends GenericService<User, UserDTO>{
    protected UserService(UserRepository userRepository, UserMapper userMapper) {
        super(userRepository , userMapper);
    }
}
