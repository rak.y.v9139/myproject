package com.example.myproject.MyProject.service;

import com.example.myproject.MyProject.dto.OwnerDTO;
import com.example.myproject.MyProject.mapper.GenericMapper;
import com.example.myproject.MyProject.mapper.OwnerMapper;
import com.example.myproject.MyProject.model.Owner;
import com.example.myproject.MyProject.repository.GenericRepository;
import com.example.myproject.MyProject.repository.OwnerRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class OwnerService extends GenericService<Owner, OwnerDTO>{

    protected OwnerService(OwnerRepository ownerRepository, OwnerMapper ownerMapper) {
        super(ownerRepository, ownerMapper);
    }
}
