package com.example.myproject.MyProject.service;

import com.example.myproject.MyProject.mapper.WorkMapper;
import com.example.myproject.MyProject.model.Work;
import com.example.myproject.MyProject.repository.WorkRepository;
import org.springframework.stereotype.Service;

@Service
public class WorkService extends GenericService<Work, com.example.myproject.MyProject.dto.WorkDTO>{
    protected WorkService(WorkRepository workRepository , WorkMapper workMapper) {
        super(workRepository , workMapper);
    }
}
