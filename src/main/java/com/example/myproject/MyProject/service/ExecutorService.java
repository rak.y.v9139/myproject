package com.example.myproject.MyProject.service;

import com.example.myproject.MyProject.dto.ExecutorDTO;
import com.example.myproject.MyProject.mapper.ExecutorMapper;
import com.example.myproject.MyProject.mapper.GenericMapper;
import com.example.myproject.MyProject.model.Executor;
import com.example.myproject.MyProject.repository.ExecutorRepository;
import com.example.myproject.MyProject.repository.GenericRepository;
import org.springframework.stereotype.Service;

@Service
public class ExecutorService extends GenericService<Executor, ExecutorDTO> {

    protected ExecutorService(ExecutorRepository executorRepository, ExecutorMapper executorMapper) {
        super(executorRepository , executorMapper);
    }
}
