package com.example.myproject.MyProject.dto;

import com.example.myproject.MyProject.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class ExecutorDTO extends GenericDTO {

    private User userId;
    private Integer salary;
    private Set<Long> worksId;
}
