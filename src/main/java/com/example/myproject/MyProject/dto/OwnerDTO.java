package com.example.myproject.MyProject.dto;

import com.example.myproject.MyProject.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class OwnerDTO extends GenericDTO{
    private User usersId;
    private String title;
    private String company;
}

