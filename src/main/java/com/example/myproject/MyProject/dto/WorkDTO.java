package com.example.myproject.MyProject.dto;

import com.example.myproject.MyProject.model.Executor;
import com.example.myproject.MyProject.model.Order;
import com.example.myproject.MyProject.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class WorkDTO extends GenericDTO{
    private Order orderId;
    private LocalDate startWork;
    private LocalDate finishWork;
    private Integer priceWork;
    private User userId;
    private Set<Executor> executorsId;

}
