package com.example.myproject.MyProject.dto;

import com.example.myproject.MyProject.model.Owner;
import com.example.myproject.MyProject.model.User;
import com.example.myproject.MyProject.model.Work;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class OrderDTO extends GenericDTO{
    private Owner ownerId;
    private String address;
    private LocalDate startOrder;
    private LocalDate finishOrder;
    private User userId;
    private Set<Work> worksId;

}
