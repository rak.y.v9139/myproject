package com.example.myproject.MyProject.dto;

import com.example.myproject.MyProject.model.Order;
import com.example.myproject.MyProject.model.Work;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class UserDTO extends GenericDTO{
    private String login;
    private String password;
    private String firstName;
    private String middleName;
    private String lastName;
    private LocalDate birthDate;
    private String phone;
    private Set<Order> ordersId;
    private Set<Work> works;
}
