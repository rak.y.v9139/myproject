package com.example.myproject.MyProject.controller;

import com.example.myproject.MyProject.dto.ExecutorDTO;
import com.example.myproject.MyProject.model.Executor;
import com.example.myproject.MyProject.service.ExecutorService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/executor")
@Tag(name = "Исполнители", description = "Контроллер для работы с исполнителями")
public class ExecutorController extends GenericController <Executor , ExecutorDTO> {
    public ExecutorController(ExecutorService executorService) {
        super(executorService);
    }
}
