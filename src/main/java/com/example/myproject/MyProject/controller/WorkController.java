package com.example.myproject.MyProject.controller;

import com.example.myproject.MyProject.dto.WorkDTO;
import com.example.myproject.MyProject.model.Work;
import com.example.myproject.MyProject.service.GenericService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/work")
@Tag(name = "Работа", description = "Контроллер для работы с работой")
public class WorkController extends GenericController<Work, WorkDTO> {
    public WorkController(GenericService<Work, WorkDTO> genericService) {
        super(genericService);
    }
}
