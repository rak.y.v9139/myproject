package com.example.myproject.MyProject.controller;

import com.example.myproject.MyProject.dto.OrderDTO;
import com.example.myproject.MyProject.model.Order;
import com.example.myproject.MyProject.service.GenericService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/order")
@Tag(name = "Заказ", description = "Контроллер для работы с заказами")
public class OrderController extends GenericController<Order, OrderDTO> {
    public OrderController(GenericService<Order, OrderDTO> genericService) {
        super(genericService);
    }
}
