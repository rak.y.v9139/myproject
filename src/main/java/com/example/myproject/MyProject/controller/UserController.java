package com.example.myproject.MyProject.controller;

import com.example.myproject.MyProject.dto.UserDTO;
import com.example.myproject.MyProject.model.User;
import com.example.myproject.MyProject.service.UserService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/user")
@Tag(name = "Пользователи", description = "Контроллер для работы с пользователями")
public class UserController extends GenericController<User, UserDTO> {
    public UserController(UserService userService) {
        super(userService);
    }
}
