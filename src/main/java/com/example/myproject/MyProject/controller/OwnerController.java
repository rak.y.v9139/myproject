package com.example.myproject.MyProject.controller;

import com.example.myproject.MyProject.dto.OwnerDTO;
import com.example.myproject.MyProject.model.Owner;
import com.example.myproject.MyProject.service.GenericService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/owner")
@Tag(name = "Заказчик", description = "Контроллер для работы с заказчиками")
public class OwnerController extends GenericController<Owner, OwnerDTO> {
    public OwnerController(GenericService<Owner, OwnerDTO> genericService) {
        super(genericService);
    }
}
