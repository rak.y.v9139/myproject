package com.example.myproject.MyProject.repository;

import com.example.myproject.MyProject.model.Work;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkRepository extends GenericRepository<Work>{
}
