package com.example.myproject.MyProject.repository;

import com.example.myproject.MyProject.model.Owner;
import org.springframework.stereotype.Repository;

@Repository
public interface OwnerRepository extends GenericRepository<Owner>{
}
