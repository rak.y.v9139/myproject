package com.example.myproject.MyProject.repository;

import com.example.myproject.MyProject.model.Executor;
import org.springframework.stereotype.Repository;

@Repository
public interface ExecutorRepository extends GenericRepository<Executor>{
}
