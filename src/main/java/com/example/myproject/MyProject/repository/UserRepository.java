package com.example.myproject.MyProject.repository;

import com.example.myproject.MyProject.model.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends GenericRepository<User>{
}
