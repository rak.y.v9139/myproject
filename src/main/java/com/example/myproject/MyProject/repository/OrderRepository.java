package com.example.myproject.MyProject.repository;

import com.example.myproject.MyProject.model.Order;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends GenericRepository<Order> {
}
