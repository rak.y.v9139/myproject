package com.example.myproject.MyProject.mapper;

import com.example.myproject.MyProject.dto.ExecutorDTO;
import com.example.myproject.MyProject.model.Executor;
import com.example.myproject.MyProject.model.GenericModel;
import com.example.myproject.MyProject.repository.ExecutorRepository;
import com.example.myproject.MyProject.repository.WorkRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class ExecutorMapper extends GenericMapper<Executor, ExecutorDTO> {


    private final WorkRepository workRepository;

    protected ExecutorMapper(ModelMapper modelMapper,
                             WorkRepository workRepository){
        super(modelMapper, Executor.class, ExecutorDTO.class);
        this.workRepository = workRepository;
    }
    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(Executor.class, ExecutorDTO.class)
                .addMappings(m -> m.skip(ExecutorDTO::setWorksId)).setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(ExecutorDTO.class, Executor.class)
                .addMappings(m -> m.skip(Executor::setWorks)).setPostConverter(toEntityConverter());

    }

    @Override
    protected void mapSpecificFields(ExecutorDTO source, Executor destination) {
        if (!Objects.isNull(source.getWorksId())){
            destination.setWorks(new HashSet<>(workRepository.findAllById(source.getWorksId())));
        }
        else {
            destination.setWorks(Collections.emptySet());
        }


    }

    @Override
    protected void mapSpecificFields(Executor source, ExecutorDTO destination) {
        destination.setWorksId(getIds(source));

    }

    @Override
    protected Set<Long> getIds(Executor executor) {
        return Objects.isNull(executor) || Objects.isNull(executor.getWorks())
                ? null
                : executor.getWorks().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }


}
