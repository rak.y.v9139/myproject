package com.example.myproject.MyProject.mapper;

import com.example.myproject.MyProject.dto.OwnerDTO;
import com.example.myproject.MyProject.model.Owner;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class OwnerMapper extends GenericMapper<Owner , OwnerDTO>{

    protected OwnerMapper(ModelMapper modelMapper){
        super (modelMapper, Owner.class , OwnerDTO.class);
    }

    @Override
    protected void setupMapper() {

    }

    @Override
    protected void mapSpecificFields(OwnerDTO source, Owner destination) {

    }

    @Override
    protected void mapSpecificFields(Owner source, OwnerDTO destination) {

    }

    @Override
    protected Set<Long> getIds(Owner entity) {
        return null;
    }
}
