package com.example.myproject.MyProject.mapper;

import com.example.myproject.MyProject.dto.OrderDTO;
import com.example.myproject.MyProject.model.Order;
import com.example.myproject.MyProject.repository.OwnerRepository;
import com.example.myproject.MyProject.repository.UserRepository;
import com.example.myproject.MyProject.repository.WorkRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;


import java.util.Set;

@Component
public class OrderMapper extends  GenericMapper<Order, OrderDTO> {

    private final UserRepository userRepository;
    private final OwnerRepository ownerRepository;
    private final WorkRepository workRepository;

    protected OrderMapper(ModelMapper modelMapper , UserRepository userRepository, WorkRepository workRepository, OwnerRepository ownerRepository) {
        super(modelMapper, Order.class, OrderDTO.class);
        this.userRepository = userRepository;
        this.ownerRepository = ownerRepository;
        this.workRepository = workRepository;
    }

    @PostConstruct
    protected void setupMapper() {
        super.modelMapper.createTypeMap(Order.class, OrderDTO.class)
                .addMappings(m -> m.skip(OrderDTO::setUserId)).setPostConverter(toDtoConverter())
                .addMappings(m -> m.skip(OrderDTO::setWorksId)).setPostConverter(toDtoConverter())
                .addMappings(m -> m.skip(OrderDTO::setOwnerId)).setPostConverter(toDtoConverter());

        super.modelMapper.createTypeMap(OrderDTO.class, Order.class)
                .addMappings(m -> m.skip(Order::setUser)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Order::setWorks)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Order::setOwner)).setPostConverter(toEntityConverter());

    }

    @Override
    protected void mapSpecificFields(OrderDTO source, Order destination) {

    }

    @Override
    protected void mapSpecificFields(Order source, OrderDTO destination) {

    }

    @Override
    protected Set<Long> getIds(Order entity) {
        return null;
    }
}


