package com.example.myproject.MyProject.mapper;

import com.example.myproject.MyProject.dto.UserDTO;
import com.example.myproject.MyProject.model.User;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class UserMapper extends GenericMapper<User, UserDTO>{

    protected UserMapper(ModelMapper modelMapper){
        super(modelMapper, User.class, UserDTO.class);
    }

    @Override
    protected void setupMapper() {

    }

    @Override
    protected void mapSpecificFields(UserDTO source, User destination) {

    }

    @Override
    protected void mapSpecificFields(User source, UserDTO destination) {

    }

    @Override
    protected Set<Long> getIds(User entity) {
        return null;
    }
}
