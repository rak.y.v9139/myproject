package com.example.myproject.MyProject.mapper;

import com.example.myproject.MyProject.dto.GenericDTO;
import com.example.myproject.MyProject.model.GenericModel;


import java.util.List;

public interface Mapper<E extends GenericModel, D extends GenericDTO> {
    E toEntity(D dto);

    List<E> toEntities(List<D> dtos);

    D toDTO(E entity);

    List<D> toDTOs(List<E> entities);
}
