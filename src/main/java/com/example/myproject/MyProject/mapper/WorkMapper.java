package com.example.myproject.MyProject.mapper;

import com.example.myproject.MyProject.dto.WorkDTO;
import com.example.myproject.MyProject.model.Work;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class WorkMapper extends GenericMapper<Work, WorkDTO> {

    protected WorkMapper(ModelMapper modelMapper){
        super(modelMapper, Work.class, WorkDTO.class);
    }

    @Override
    protected void setupMapper() {

    }

    @Override
    protected void mapSpecificFields(WorkDTO source, Work destination) {

    }

    @Override
    protected void mapSpecificFields(Work source, WorkDTO destination) {

    }

    @Override
    protected Set<Long> getIds(Work entity) {
        return null;
    }
}
